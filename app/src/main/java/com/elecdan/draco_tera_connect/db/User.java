package com.elecdan.draco_tera_connect.db;

import com.elecdan.draco_tera_connect.Login;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

// User class
@Entity(foreignKeys = {
        @ForeignKey(
                entity = Group.class,
                parentColumns = "id",
                childColumns = "group_id",
                onDelete = CASCADE
        )},
        indices = {
                @Index(value = "group_id"),
                @Index(value = "username", unique = true)
        })
public class User {
    // user id
    @PrimaryKey(autoGenerate = true)
    public Integer id;

    // id of the last profile opened in the app
    public int lastProfile;

    // group id
    @ColumnInfo(name = "group_id")
    public int group_id;

    // username
    @ColumnInfo(name = "username")
    public String username;

    // password
    @ColumnInfo(name = "password")
    public String password;

    public boolean selected;

    public User(int group_id, String username, String password) {
        this.group_id = group_id;
        this.username = username;
        this.password = password;
        this.lastProfile = -1;
    }

    static User init() {
        String pw = Login.hash("", "admin");
        return new User(1, "admin", pw);
    }
}
