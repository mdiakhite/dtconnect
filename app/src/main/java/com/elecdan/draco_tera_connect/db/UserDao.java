package com.elecdan.draco_tera_connect.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

// settings data access object
@Dao
public interface UserDao {
    // insert an user
    @Insert
    void add(User user);

    @Update
    void update(User user);

    @Query("SELECT * FROM User WHERE username = :username AND password = :password")
    User[] authenticate(String username, String password);

    @Query("SELECT * FROM User WHERE username = :username")
    User[] get(String username);

    @Query("SELECT * FROM User WHERE group_id = :group_id")
    User[] getByGroup(int group_id);

    @Query("SELECT * FROM User " +
            "INNER JOIN `Group` ON `Group`.id = User.group_id " +
            "WHERE privilege = 0")
    User[] getAdmins();

    @Delete
    void delete(User user);

    @Query("SELECT * FROM User")
    User[] getAll();
}
