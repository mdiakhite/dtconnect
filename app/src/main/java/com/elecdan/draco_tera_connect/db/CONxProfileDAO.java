package com.elecdan.draco_tera_connect.db;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

// console data access object
@Dao
public interface CONxProfileDAO {
    // add a console to the database
    @Insert
    void add(CONxProfile conx);

    // update an existing console
    @Query("UPDATE CONxProfile " +
            "SET x = :x, y = :y, width = :w, height = :h " +
            "WHERE id = :id")
    void updateValues(int id, int x, int y, int w, int h);

    // retrieve all the consoles
    @Transaction
    @Query("SELECT CONxProfile.*, CON.name, CON.cpu " +
            "FROM CONxProfile " +
            "INNER JOIN CON on CONxProfile.con_id = CON.id")
    List<ConJoin> getCons();

    // retrieve the consoles associated to a profile
    @Transaction
    @Query("SELECT CONxProfile.*, CON.name, CON.cpu " +
            "FROM CONxProfile " +
            "INNER JOIN CON on CONxProfile.con_id = CON.id " +
            "WHERE CONxProfile.profile_id = :profile_id")
    List<ConJoin> getConsByProfile(int profile_id);

    // retrieve the consoles associated to a profile
    @Transaction
    @Query("SELECT CON.name " +
            "FROM CON " +
            "WHERE CON.id NOT IN (" +
            "SELECT con_id FROM CONxProfile WHERE profile_id = :profile_id)")
    List<String> getConsNotInProfile(int profile_id);

    // retrieve a specific console
    @Transaction
    @Query("SELECT CONxProfile.*, CON.name, CON.cpu " +
            "FROM CONxProfile " +
            "INNER JOIN CON on CONxProfile.con_id = CON.id " +
            "WHERE CONxProfile.id = :id")
    List<ConJoin> getConById(int id);

    // remove all the associations of consoles to a specific profile
    @Query("DELETE FROM CONxProfile WHERE profile_id = :profile_id")
    void removeProfile(int profile_id);

    // remove all the associations of consoles to a specific profile
    @Query("DELETE FROM CONxProfile WHERE id = :id")
    void removeCon(int id);

    // drop the entire table
    @Query("DELETE FROM CONxProfile")
    void delete();
}
