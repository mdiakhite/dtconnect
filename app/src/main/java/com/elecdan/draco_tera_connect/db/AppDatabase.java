package com.elecdan.draco_tera_connect.db;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

// Database class
@Database(entities = {Profile.class, CON.class, CPU.class, Settings.class, CONxProfile.class, User.class, Group.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public static AppDatabase INSTANCE;

    // Data Access Objects

    // Profiles
    public abstract ProfileDAO ProfileDao();

    // Consoles
    public abstract CONDAO CONDao();

    // Sources
    public abstract CPUDAO CPUDao();

    // General Settings
    public abstract SettingsDAO SettingsDAO();

    // Consoles x Profiles joining table (many to many relation)
    public abstract CONxProfileDAO CONxProfileDAO();

    // Users
    public abstract UserDao UserDao();

    // Groups
    public abstract GroupDao GroupDao();

    public synchronized static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = buildDatabase(context);
        }
        return INSTANCE;
    }

    private static AppDatabase buildDatabase(final Context context) {
        return Room.databaseBuilder(context,
                AppDatabase.class,
                "MyDB")
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                getInstance(context).SettingsDAO().add(Settings.init());
                                getInstance(context).GroupDao().addAll(Group.init());
                                getInstance(context).UserDao().add(User.init());
                            }
                        });
                    }
                })
                .build();
    }
}
