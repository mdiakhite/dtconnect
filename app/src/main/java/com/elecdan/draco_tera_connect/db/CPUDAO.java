package com.elecdan.draco_tera_connect.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

// cpu data access object
@Dao
public interface CPUDAO {
    @Insert
    void addCpu(CPU cpu);

    @Update
    void updateCpu(CPU cpu);

    // retrieve all the cpus
    @Query("SELECT * FROM CPU")
    CPU[] getAll();

    // remove a cpu
    @Query("DELETE FROM CPU WHERE id = :id")
    void remove(byte[] id);

    // drop the entire table
    @Query("DELETE FROM CPU")
    void delete();
}
