package com.elecdan.draco_tera_connect.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// Console class
@Entity
public class CON {
    // console id coming from the matrix
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    public byte[] id;

    public CON(byte[] id, String name) {
        this.id = id;
        this.name = name;
    }

    // console name coming from the matrix
    @ColumnInfo(name = "name")
    public String name;

    // name of the cpu the console is connected to
    @ColumnInfo(name = "cpu")
    public String cpu;
}
