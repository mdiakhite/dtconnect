package com.elecdan.draco_tera_connect.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// CPU class
@Entity
public class CPU {
    // cpu id coming from the matrix
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    public byte[] id;

    // name of the cpu
    @ColumnInfo(name = "name")
    public String name;

    // type of the cpu
    @ColumnInfo(name = "type")
    public short type;

    public CPU(byte[] id, String name, short type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }
}
