package com.elecdan.draco_tera_connect.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

// settings data access object
@Dao
public interface SettingsDAO {
    // initialize the settings
    @Insert
    void add(Settings s);

    // retrieve the settings
    @Query("SELECT * FROM Settings WHERE id = 1")
    Settings[] get();

    // update the settings
    @Query("UPDATE Settings SET ip = :ip, port = :port, login = :login WHERE id = 1")
    void update(String ip, int port, boolean login);
}
