package com.elecdan.draco_tera_connect.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// Group class
@Entity
public class Group {
    // group id
    @PrimaryKey(autoGenerate = true)
    public int id;

    // group privilege
    @ColumnInfo(name = "privilege")
    public int privilege;

    // group name
    @ColumnInfo(name = "name")
    public String name;

    public Group(int privilege, String name) {
        this.privilege = privilege;
        this.name = name;
    }

    static Group[] init() {
        return new Group[]{
                new Group(0, "Admin")
        };
    }
}
