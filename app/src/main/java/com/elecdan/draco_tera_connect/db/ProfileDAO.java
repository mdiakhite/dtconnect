package com.elecdan.draco_tera_connect.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

// profile data access object
@Dao
public interface ProfileDAO {
    // add a profile
    @Insert
    void addProfile(Profile profile);

    // retrieve all the profiles
    @Query("SELECT * FROM Profile")
    Profile[] getProfiles();

    // retrieve the profiles of a user
    @Query("SELECT * FROM Profile WHERE user_id = :user_id")
    Profile[] getProfilesOfUser(Integer user_id);

    // retrieve a specific profile
    @Query("SELECT * FROM Profile WHERE name = :name")
    Profile[] getProfileFromName(String name);

    // delete a profile
    @Query("DELETE FROM Profile WHERE name = :name")
    void deleteByName(String name);

    // update the name and information of a profile
    @Query("UPDATE Profile SET name = :name, information = :information WHERE id = :id")
    void updateNameInfo(int id, String name, String information);

    // drop the entire table
    @Query("DELETE FROM Profile")
    void delete();
}
