package com.elecdan.draco_tera_connect.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = {
        @ForeignKey(
                entity = Profile.class,
                parentColumns = "id",
                childColumns = "profile_id",
                onDelete = CASCADE
        ),
        @ForeignKey(
                entity = CON.class,
                parentColumns = "id",
                childColumns = "con_id",
                onDelete = CASCADE
        )},
        indices = {
                @Index(value = "profile_id"),
                @Index(value = "con_id")
        })
public class CONxProfile {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "profile_id")
    public int profile_id;

    @ColumnInfo(name = "con_id")
    public byte[] con_id;

    // drawing values for the canvas layout

    // console top left corner x coordinate
    public int x;

    // console top left corner y coordinate
    public int y;

    // console width
    public int width;

    // console height
    public int height;

    public CONxProfile(int profile_id, byte[] con_id, int x, int y, int width, int height) {
        this.profile_id = profile_id;
        this.con_id = con_id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}
