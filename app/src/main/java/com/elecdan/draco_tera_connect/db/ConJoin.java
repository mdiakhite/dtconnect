package com.elecdan.draco_tera_connect.db;

public class ConJoin {
    public int id;
    public int profile_id;
    public byte[] con_id;
    public int x;
    public int y;
    public int width;
    public int height;
    // values from CON
    public String name;
    public String cpu;
}
