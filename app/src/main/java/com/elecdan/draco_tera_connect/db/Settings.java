package com.elecdan.draco_tera_connect.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

// Settings class
@Entity
public class Settings {
    // settings id
    @PrimaryKey
    public int id;

    // port used to connect to the matrix
    public int port;

    // ip address used to connect to the matrix
    public String ip;


    public boolean login;

    public Settings(int port, String ip, int id, boolean login) {
        this.port = port;
        this.ip = ip;
        this.id = id;
        this.login = login;
    }

    static Settings init() {
        return new Settings(5555, "192.168.100.99", 1, false);
    }
}
