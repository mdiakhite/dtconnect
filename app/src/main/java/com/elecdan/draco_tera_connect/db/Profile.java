package com.elecdan.draco_tera_connect.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

// Profile class
@Entity(foreignKeys = {
        @ForeignKey(
                entity = User.class,
                parentColumns = "id",
                childColumns = "user_id",
                onDelete = CASCADE
        )},
        indices = {
                @Index(value = "user_id")
        })
public class Profile {
    // profile id
    @PrimaryKey(autoGenerate = true)
    public int id;

    // profile name
    @ColumnInfo(name = "name")
    public String name;

    // user id
    @ColumnInfo(name = "user_id")
    public Integer user_id;

    // profile extra information
    @ColumnInfo(name = "information")
    public String information;

    public Profile(String name, String information, Integer user_id) {
        this.name = name;
        this.information = information;
        this.user_id = user_id;
    }
}
