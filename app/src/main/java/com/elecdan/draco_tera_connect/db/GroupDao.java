package com.elecdan.draco_tera_connect.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

// settings data access object
@Dao
public interface GroupDao {
    // insert a group
    @Insert
    void add(Group group);

    @Insert
    void addAll(Group... groups);

    @Delete
    void delete(Group group);

    @Update
    void update(Group group);

    @Query("SELECT * FROM `Group` WHERE name = :name")
    Group[] getByName(String name);

    @Query("SELECT * FROM `Group`")
    Group[] getAll();

    @Query("SELECT * FROM `Group` WHERE id = :id")
    Group[] getById(int id);
}
