package com.elecdan.draco_tera_connect.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

// console data access object
@Dao
public interface CONDAO {
    // add a console to the database
    @Insert
    void addCon(CON con);

    // update an existing console
    @Update
    void updateCon(CON con);

    // retrieve all the consoles
    @Query("SELECT * FROM CON")
    CON[] getCons();

    // retrieve a specific console
    @Query("SELECT * FROM CON WHERE id = :id")
    CON[] getConById(byte[] id);

    // retrieve a specific console
    @Query("SELECT * FROM CON WHERE name = :name")
    CON[] getConByName(String name);

    @Query("DELETE FROM CON WHERE id = :id")
    void deleteById(byte[] id);

    // drop the entire table
    @Query("DELETE FROM CON")
    void delete();
}
