package com.elecdan.draco_tera_connect.matrix;

public class Extender {
    private String name;
    private byte[] id, cpu_id;
    private int type, position; //type 1==CPU & 2==CON
    private boolean isSelected;
    public short cpuType;

    Extender() {
        name = "Extender";
        id = new byte[2];
        setDefaultCpuId();
        type = 0;
        position = 0;
        isSelected = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.replaceAll("\\p{C}", "");
    }

    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    byte[] getCpu_id() {
        return cpu_id;
    }

    void setCpu_id(byte[] _cpu_id) {
        this.cpu_id = new byte[_cpu_id.length];
        System.arraycopy(_cpu_id, 0, this.cpu_id, 0, _cpu_id.length);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    void toggleSelected() {
        isSelected = !isSelected;
    }

    boolean isEqual(Extender ext) {
        for (int i = 0; i < ext.getId().length && i < this.getId().length; i++) {
            if (this.getId()[i] != ext.getId()[i])
                return false;
        }
        return true;
    }

    boolean isEqualId(byte[] _id) {
        for (int i = 0; this.getId() != null && _id != null && i < _id.length && i < this.getId().length; i++) {
            if (this.getId()[i] != _id[i])
                return false;
        }
        return true;
    }

    private void setDefaultCpuId() {
        cpu_id = new byte[2];
        for (int i = 0; i < cpu_id.length; i++) {
            cpu_id[i] = 0x00;
        }
    }
}
