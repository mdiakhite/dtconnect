package com.elecdan.draco_tera_connect.matrix;

import com.elecdan.draco_tera_connect.myUtilities.NetworkUtilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.HashSet;

public class DracoMatrix {
    private Socket socket;
    private String[] cpuList, conList, userList;
    private int portNumber, cpuCount, conCount, userCount, connectMode;
    private String ipAddress;
    private Extender[] extenders;

    public DracoMatrix(String _ipAddress, int _portNumber) {
        this.socket = null;
        this.ipAddress = _ipAddress;
        this.portNumber = _portNumber;
        this.cpuCount = 0;
        this.conCount = 0;
        this.userCount = 0;
        this.cpuList = new String[0];
        this.conList = new String[0];
        this.userList = new String[0];
        this.extenders = new Extender[0];
        this.connectMode = 0;
    }

    public DracoMatrix(DracoMatrix _matrix) {
        this.socket = null;
        this.setIpAddress(_matrix.getIpAddress());
        this.setPortNumber(_matrix.getPortNumber());
        this.setCpuCount(_matrix.getCpuCount());
        this.setConCount(_matrix.getConCount());
        this.setUserCount(_matrix.getCpuCount());
        this.setCpuList(_matrix.getCpuList());
        this.setConList(_matrix.getConList());
        this.setUserList(_matrix.getUserList());
        this.setExtenders(_matrix.getExtenders());
        this.setConnectMode(_matrix.getConnectMode());
    }

    public String[] getCpuList() {
        return cpuList;
    }

    private void setCpuList(String[] _cpuList) {
        this.cpuList = new String[_cpuList.length];
        System.arraycopy(_cpuList, 0, this.cpuList, 0, _cpuList.length);
    }

    public String[] getConList() {
        return conList;
    }

    public HashSet<Short> getConnectedCpus() {
        HashSet<Short> res = new HashSet<>();
        for (Extender e : extenders)
            if (e.getType() == 2) {
                ByteBuffer wrapped = ByteBuffer.wrap(e.getCpu_id());
                res.add(wrapped.getShort());
            }
        return res;
    }

    public byte[] getCpuIdByConName(String conName) {
        for (Extender e : extenders)
            if (e.getType() == 2 && e.getName().equals(conName))
                return e.getCpu_id();
        return null;
    }

    public int getPositionByConId(String name) {
        int res = 0;
        for (Extender e : extenders) {
            if (e.getType() == 2) {
                if (e.getName().equals(name))
                    return res;
                ++res;
            }
        }
        return -1;
    }

    private void setConList(String[] _conList) {
        this.conList = new String[_conList.length];
        System.arraycopy(_conList, 0, this.conList, 0, _conList.length);
    }

    private String[] getUserList() {
        return userList;
    }

    private void setUserList(String[] _userList) {
        this.userList = new String[_userList.length];
        System.arraycopy(_userList, 0, this.userList, 0, _userList.length);
    }

    private int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    private int getCpuCount() {
        return cpuCount;
    }

    private void setCpuCount(int cpuCount) {
        this.cpuCount = cpuCount;
    }

    private int getConCount() {
        return conCount;
    }

    private void setConCount(int conCount) {
        this.conCount = conCount;
    }

    public int getUserCount() {
        return userCount;
    }

    private void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    private String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Extender[] getExtenders() {
        return extenders;
    }

    public int getConnectMode() {
        return connectMode;
    }

    public void setConnectMode(int connectMode) {
        this.connectMode = connectMode;
    }

    private void setExtenders(Extender[] _extenders) {
        extenders = new Extender[_extenders.length];
        System.arraycopy(_extenders, 0, extenders, 0, _extenders.length);
    }

    private void addExtender(Extender ext) {
        printExtender(ext);
        System.out.println("Extenders count: " + extenders.length);
        Extender[] tmp = new Extender[extenders.length];
        System.arraycopy(extenders, 0, tmp, 0, extenders.length);
        extenders = new Extender[extenders.length + 1];
        System.arraycopy(tmp, 0, extenders, 0, tmp.length);
        extenders[extenders.length - 1] = ext;
        System.out.println("Extenders count incremented: " + extenders.length);
    }

    public void resetExtenders() {
        this.extenders = new Extender[0];
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket() {
        this.socket = NetworkUtilities.connectMatrix(ipAddress, portNumber);
    }

    protected void addCon(String str) {
        String[] tmp = conList;
        conList = new String[conList.length + 1];
        System.arraycopy(tmp, 0, conList, 0, conList.length - 1);
        conList[conList.length - 1] = str;
    }

    public void resetCons() {
        this.conList = new String[0];
    }

    private void addCpu(String str) {
        String[] tmp = cpuList;
        cpuList = new String[cpuList.length + 1];
        System.arraycopy(tmp, 0, cpuList, 0, cpuList.length - 1);
        cpuList[cpuList.length - 1] = str;
    }

    public void resetCpus() {
        this.cpuList = new String[0];
    }

    protected void addUser(String str) {
        String[] tmp = userList;
        userList = new String[userCount++];
        System.arraycopy(tmp, 0, userList, 0, userCount - 1);
        userList[userCount - 1] = str;
    }

    public void resetUsers() {
        this.userList = new String[0];
    }

    public void loadCpuList() {
        try {
            final InputStream is = this.socket.getInputStream();
            final OutputStream os = this.socket.getOutputStream();
            byte[] b = new byte[8192];
            int endOfLine;
            // Get CPU List
            os.write(0x1B); // ESP
            os.write(0x5B); // [
            os.write(0x67); // g
            os.write(0x07); // LSB Size
            os.write(0x00); // MSB Size
            os.write(0x00); // LSB First CPU ID (ID=0000)
            os.write(0x00); // MSB First CPU ID
            os.flush();
            endOfLine = is.read(b); // Read the stream
            System.out.println("Reading CPU list from matrix");
            if (endOfLine != -1) { // No data available
                for (int i = 9; i < endOfLine; i += 24) {
                    //Each device ID consist of 2 byte with LSB the first byte and the MSB the second one
                    //We first get the CPU ID then its name
                    Extender tmp = new Extender();
                    byte[] id = new byte[2];
                    id[0] = b[i];
                    id[1] = b[i + 1];
                    //System.out.println();
                    //The name of the device consist of 24 bytes
                    StringBuilder name = new StringBuilder();
                    for (int j = i + 2; j < i + 24; j++) {
                        //System.out.print(String.format("%c", b[j]));
                        name.append(String.format("%c", b[j]));
                    }
                    addCpu(name.toString());
                    tmp.setId(id);
                    tmp.setName(name.toString());
                    tmp.setType(1);
                    addExtender(tmp);
                }
            } else {
                System.out.println("No available data");
            }
            System.out.println("Finished reading");
            printTable(cpuList);
            printExtenders(extenders);
            // End Get CPU List
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadConList() {
        try {
            final InputStream is = this.socket.getInputStream();
            final OutputStream os = this.socket.getOutputStream();
            byte[] b = new byte[8192];
            int endOfLine;
            // Get CON List
            os.write(0x1B); // ESP
            os.write(0x5B); // [
            os.write(0x68); // h
            os.write(0x07); // LSB Size
            os.write(0x00); // MSB Size
            os.write(0x00); // LSB First CON ID (ID=0000)
            os.write(0x00); // MSB First CON ID
            os.flush();
            endOfLine = is.read(b); // Read the stream
            System.out.println("Reading CON list from matrix");
            if (endOfLine != -1) { // No data available
                for (int i = 9; i < endOfLine; i += 24) {
                    //Each device ID consist of 2 byte with LSB the first byte and the MSB the second one
                    //We first get the CON ID then its name
                    Extender tmp = new Extender();
                    byte[] id = new byte[2];
                    id[0] = b[i];
                    id[1] = b[i + 1];
                    //System.out.print(" ");
                    //The name of the device consist of 24 bytes
                    StringBuilder name = new StringBuilder();
                    for (int j = i + 2; j < i + 24; j++) {
                        //System.out.print(String.format("%c", b[j]));
                        name.append(String.format("%c", b[j]));
                    }
                    System.out.println();
                    addCon(name.toString());
                    tmp.setId(id);
                    tmp.setName(name.toString());
                    tmp.setType(2);
                    addExtender(tmp);
                }
            } else {
                System.out.println("No available data");
            }
            System.out.println("Finished reading");
            printTable(conList);
            printExtenders(extenders);
            // End Get CON List
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printTable(String[] table) {
        for (String aTable : table) System.out.print(aTable);
        System.out.println();
    }

    private void printExtenders(Extender[] _extenders) {
        for (Extender _extender : _extenders) {
            System.out.print(idToString(_extender.getId()) + " " + _extender.getName());
        }
        System.out.println();
    }

    private void printExtender(Extender ext) {
        System.out.println(idToString(ext.getId()) + " " + ext.getName());
    }

    private String idToString(byte[] _id) {
        StringBuilder id = new StringBuilder();
        for (byte a_id : _id) id.append(String.format("%02x", a_id));
        return id.toString();
    }

    public void selectExtender(Extender ext) {
        if (ext.getType() == 1) {
            for (Extender extender : extenders) {
                if (extender.getType() == 1 && !ext.isEqual(extender)) {
                    extender.setSelected(false);
                }
            }
            ext.toggleSelected();
        } else {
            ext.toggleSelected();
        }
    }

    public Extender[] selectedCons() {
        Extender[] tmp = new Extender[extenders.length];
        int j = 0;
        for (Extender extender : extenders) {
            if (extender.getType() == 2 && extender.isSelected()) {
                tmp[j] = extender;
                j++;
            }
        }
        Extender[] selectedCons = new Extender[j];
        System.arraycopy(tmp, 0, selectedCons, 0, j);
        printExtenders(selectedCons);
        return selectedCons;
    }

    public Extender[] getConUnits() {
        Extender[] tmp = new Extender[extenders.length];
        int j = 0;
        for (Extender extender : extenders) {
            if (extender.getType() == 2) {
                tmp[j] = extender;
                j++;
            }
        }
        Extender[] consoles = new Extender[j];
        System.arraycopy(tmp, 0, consoles, 0, j);
        return consoles;
    }

    public Extender[] getCpuUnits() {
        Extender[] tmp = new Extender[extenders.length];
        int j = 0;
        for (Extender extender : extenders) {
            if (extender.getType() == 1) {
                tmp[j] = extender;
                j++;
            }
        }
        Extender[] cpus = new Extender[j];
        System.arraycopy(tmp, 0, cpus, 0, j);
        return cpus;
    }

    public void deselectAll() {
        for (Extender extender : extenders) {
            extender.setSelected(false);
        }
    }

    public Extender selectedCpu() {
        for (Extender extender : extenders) {
            if (extender.getType() == 1 && extender.isSelected()) {
                return extender;
            }
        }
        return null;
    }

    public String getExtenderNameUsingId(byte[] _id) {
        for (Extender extender : extenders) {
            if (extender.isEqualId(_id))
                return extender.getName();
        }
        return null;
    }

    public void connectConsToCpu(Extender[] consoles, Extender cpu, int mode) {
        try {
            final InputStream is = this.socket.getInputStream();
            final OutputStream os = this.socket.getOutputStream();
            byte[] b = new byte[8192];
            int endOfLine;
            // Print select consoles and cpus
            if (consoles.length != 0) {
                System.out.print("Selected CON (s): ");
                printExtenders(consoles);
            }
            if (cpu != null) {
                System.out.print("Selected CPU: ");
                printExtender(cpu);
            }
            //Send command to matrix: 0 = Disconnect, 1 = video only, 2 = full connect, 3 = private
            switch (mode) {
                case 0: //Disconnect
                    for (Extender console : consoles) {
                        os.write(0x1B); // ESC
                        os.write(0x5B); // [
                        os.write(0x49); // I
                        os.write(0x09); // LSB Size
                        os.write(0x00); // MSB Size
                        os.write(console.getId()[0]); // LSB CON ID
                        os.write(console.getId()[1]); // MSB CON ID
                        os.write(0x00); // LSB CPU ID
                        os.write(0x00); // MSB CPU ID
                        os.flush();
                        // Read response
                        endOfLine = is.read(b);
                        if (endOfLine != -1) { // No data available ?
                            //The first 5 bytes include 'ESC', ']', 'H' plus
                            //the size of the stream consiting of the 2 remaining bytes
                            for (int i = 0; i < endOfLine; i++) {
                                //Response is ACK == 0x06 or NAK = 0x15
                                System.out.println(String.format("%02x ", b[i]) + " " + String.format("%02x ", console.getId()[1]) + " " + String.format("%02x ", console.getId()[0]));
                            }
                        } else {
                            System.out.println("No available data");
                        }
                    }
                    break;
                case 1: //Connect video only
                    for (Extender console : consoles) {
                        if (cpu != null) {
                            printExtender(cpu);
                            os.write(0x1B); // ESC
                            os.write(0x5B); // [
                            os.write(0x49); // I=0x49, M=0x4D, 0x50, Q=0x51
                            os.write(0x09); // LSB Size
                            os.write(0x00); // MSB Size
                            os.write(console.getId()[0]); // LSB CON ID
                            os.write(console.getId()[1]); // MSB CON ID
                            os.write(cpu.getId()[0]); // LSB CPU ID
                            os.write(cpu.getId()[1]); // MSB CPU ID
                            os.flush();
                            // Read response
                            endOfLine = is.read(b);
                            if (endOfLine != -1) { // No data available ?
                                //The first 5 bytes include 'ESC', ']', 'H' plus
                                //the size of the stream consiting of the 2 remaining bytes
                                for (int i = 0; i < endOfLine; i++) {
                                    //Response is ACK == 0x06 or NAK = 0x15
                                    //System.out.println(String.format("%02x", b[i]));
                                    System.out.println(String.format("%02x ", b[i]) + " " + String.format("%02x ", console.getId()[1]) + " " + String.format("%02x ", console.getId()[0]));
                                }
                            } else {
                                System.out.println("No available data");
                            }
                        }
                    }
                    break;
                case 2: //Full connect
                    for (Extender console : consoles) {
                        if (cpu != null) {
                            printExtender(cpu);
                            os.write(0x1B); // ESC
                            os.write(0x5B); // [
                            os.write(0x50); // I=0x49, M=0x4D, 0x50, Q=0x51
                            os.write(0x09); // LSB Size
                            os.write(0x00); // MSB Size
                            os.write(cpu.getId()[0]); // LSB CPU ID
                            os.write(cpu.getId()[1]); // MSB CPU ID
                            os.write(console.getId()[0]); // LSB CON ID
                            os.write(console.getId()[1]); // MSB CON ID
                            os.flush();
                            // Read response
                            endOfLine = is.read(b);
                            if (endOfLine != -1) { // No data available ?
                                //The first 5 bytes include 'ESC', ']', 'H' plus
                                //the size of the stream consiting of the 2 remaining bytes
                                for (int i = 0; i < endOfLine; i++) {
                                    //Response is ACK == 0x06 or NAK = 0x15
                                    //System.out.println(String.format("%02x", b[i]));
                                    System.out.println(String.format("%02x ", b[i]) + " " + String.format("%02x ", console.getId()[1]) + " " + String.format("%02x ", console.getId()[0]));
                                }
                            } else {
                                System.out.println("No available data");
                            }
                        }
                    }
                    break;
                case 3: //Connect private
                    for (Extender console : consoles) {
                        if (cpu != null) {
                            printExtender(cpu);
                            os.write(0x1B); // ESC
                            os.write(0x5B); // [
                            os.write(0x62); // I=0x49, M=0x4D, 0x50, Q=0x51
                            os.write(0x09); // LSB Size
                            os.write(0x00); // MSB Size
                            os.write(cpu.getId()[0]); // LSB CPU ID
                            os.write(cpu.getId()[1]); // MSB CPU ID
                            os.write(console.getId()[0]); // LSB CON ID
                            os.write(console.getId()[1]); // MSB CON ID
                            os.write(0x02); // LSB
                            os.write(0x00); // MSB
                            os.flush();
                            // Read response
                            endOfLine = is.read(b);
                            if (endOfLine != -1) { // No data available ?
                                //The first 5 bytes include 'ESC', ']', 'H' plus
                                //the size of the stream consiting of the 2 remaining bytes
                                for (int i = 0; i < endOfLine; i++) {
                                    //Response is ACK == 0x06 or NAK = 0x15
                                    //System.out.println(String.format("%02x", b[i]));
                                    System.out.println(String.format("%02x ", b[i]) + " " + String.format("%02x ", console.getId()[1]) + " " + String.format("%02x ", console.getId()[0]));
                                }
                            } else {
                                System.out.println("No available data");
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
            // Close socket and streams
            is.close();
            os.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //End Connect CPU 1001 to CON 3000
    }

    public void setConnectedCpu() {
        Extender[] conUnits = this.getConUnits();
        try {
            final InputStream is = this.socket.getInputStream();
            final OutputStream os = this.socket.getOutputStream();
            byte[] b = new byte[8192];
            int endOfLine;
            // Block 4
            //Get CPU connected to CON 3000
            for (Extender conUnit : conUnits) {
                os.write(0x1B); // ESC
                os.write(0x5B); // [
                os.write(0x48); // H
                os.write(0x07); // LSB Size
                os.write(0x00); // MSB Size
                os.write(conUnit.getId()[0]); // LSB CON ID
                os.write(conUnit.getId()[1]); // MSB CON ID
                os.flush();
                // Read response
                endOfLine = is.read(b);
                byte[] tmp = new byte[2];
                if (endOfLine != -1) { // No data available ?
                    //The first 5 bytes include 'ESC', ']', 'H' plus the size of the stream consiting of the 2 remaining bytes
                    for (int j = 5; j < endOfLine - 1; j = j + 2) {
                        //Each device ID consist of 2 byte with LSB the first byte and the MSB the second one
                        //We first get the CON ID then the CPU ID
                        System.out.print(String.format("%02x", b[j + 1]));
                        System.out.println(String.format("%02x", b[j]));
                        tmp[0] = b[j];
                        tmp[1] = b[j + 1];
                    }
                    conUnit.setCpu_id(tmp);
                } else {
                    System.out.println("No available data");
                }
            }
            //End Get CPU connected to CON 3000
            // Close socket and streams
            is.close();
            os.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
