package com.elecdan.draco_tera_connect;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.elecdan.draco_tera_connect.db.AppDatabase;
import com.elecdan.draco_tera_connect.db.CON;
import com.elecdan.draco_tera_connect.db.CONDAO;
import com.elecdan.draco_tera_connect.db.CONxProfile;
import com.elecdan.draco_tera_connect.db.CONxProfileDAO;
import com.elecdan.draco_tera_connect.db.CPU;
import com.elecdan.draco_tera_connect.db.CPUDAO;
import com.elecdan.draco_tera_connect.db.ConJoin;
import com.elecdan.draco_tera_connect.db.Profile;
import com.elecdan.draco_tera_connect.db.ProfileDAO;
import com.elecdan.draco_tera_connect.db.Settings;
import com.elecdan.draco_tera_connect.db.SettingsDAO;
import com.elecdan.draco_tera_connect.db.User;
import com.elecdan.draco_tera_connect.db.UserDao;
import com.elecdan.draco_tera_connect.matrix.DracoMatrix;
import com.elecdan.draco_tera_connect.matrix.Extender;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private ProgressBar loading;
    private DracoMatrix matrix;
    private ListView cpus_actions;
    AppDatabase db;
    CONDAO condao;
    CPUDAO cpudao;
    ProfileDAO profileDAO;
    SettingsDAO settingsDAO;
    CONxProfileDAO CONxProfileDAO;
    UserDao userdao;
    ArrayAdapter<String> profile_list;
    Spinner dynamic_profile_list;
    final Context ctx = this;
    Button btn_create, btn_delete, btn_edit, fullConnect, video_only, disconnect, disconnectAll, addConP;
    ImageView editmode, status;
    AlertDialog alertDialogAndroid;
    ArrayList<ConJoin> cons = new ArrayList<>();
    ArrayList<CPU> cpus = new ArrayList<>();
    ArrayList<Rect> rectangles = new ArrayList<>();
    Grid grid;
    User user;
    String lastProfile = "", ipAddress, username;
    boolean ok = true, editing = false, login = false;
    int portNumber, lastPosition = -1, lastX = 0, lastY = 0, touchX, touchY, privilege;
    Intent intent;

    void checkLogin() {
        if (login && userdao.get(username).length == 0) {
            Intent intent = new Intent(getApplicationContext(), Login.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                checkLogin();
            }
        });
    }

    @SuppressLint({"ClickableViewAccessibility", "WrongViewCast"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent = getIntent();
        privilege = intent.getIntExtra("privilege", 0);
        username = intent.getStringExtra("username");
        grid = findViewById(R.id.grid);
        btn_create = findViewById(R.id.btn_create);
        btn_delete = findViewById(R.id.btn_delete);
        addConP = findViewById(R.id.addCon);
        btn_edit = findViewById(R.id.btn_edit);
        cpus_actions = findViewById(R.id.cpu_act_grid);
        loading = findViewById(R.id.loading_indicator);

        // database init
        db = AppDatabase.getInstance(getApplicationContext());
        condao = db.CONDao();
        cpudao = db.CPUDao();
        profileDAO = db.ProfileDao();
        settingsDAO = db.SettingsDAO();
        CONxProfileDAO = db.CONxProfileDAO();
        userdao = db.UserDao();

        // initialize/retrieve settings + load matrix data
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Settings[] tmp = settingsDAO.get();
                ipAddress = tmp[0].ip;
                portNumber = tmp[0].port;
                login = tmp[0].login;
                User[] getUser = userdao.get(username);
                if (getUser.length == 1) {
                    user = getUser[0];
                    lastPosition = user.lastProfile;
                }
                if (login)
                    getSupportActionBar().setTitle(username);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dynamic_profile_list.setSelection(lastPosition);
                    }
                });
                matrix = new DracoMatrix(ipAddress, portNumber);
                new LoadMatrixData().execute(matrix);
            }
        });

        // 'add con' button
        addConP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ctx);
                @SuppressLint("InflateParams") final View mView = layoutInflaterAndroid.inflate(R.layout.addcon, null);
                final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ctx);
                final Spinner spin = mView.findViewById(R.id.spinner3);
                final boolean[] empty = {true};
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        Profile[] ps = profileDAO.getProfileFromName(lastProfile);
                        if (ps.length == 0)
                            return;
                        Profile p = ps[0];
                        ArrayList<String> l = new ArrayList<>(CONxProfileDAO.getConsNotInProfile(p.id));
                        final ArrayAdapter<String> con_list = new ArrayAdapter<>(ctx, android.R.layout.simple_spinner_dropdown_item, l);
                        if (l.size() > 0)
                            empty[0] = false;
                        spin.setAdapter(con_list);
                        alertDialogBuilderUserInput.setView(mView);
                        alertDialogBuilderUserInput
                                .setCancelable(false)
                                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        AsyncTask.execute(new Runnable() {
                                            @Override
                                            public void run() {
                                                Profile p = profileDAO.getProfileFromName(lastProfile)[0];
                                                CON c = condao.getConByName(spin.getSelectedItem().toString())[0];
                                                int x = 0;
                                                boolean hit = true;
                                                while (hit) {
                                                    hit = false;
                                                    for (ConJoin tmp : cons) {
                                                        if (tmp.x == x && tmp.y == 0) {
                                                            x += 8;
                                                            hit = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                CONxProfile conx = new CONxProfile(p.id, c.id, x, 0, 46, 32);
                                                CONxProfileDAO.add(conx);
                                                reloadGridView(matrix);
                                            }
                                        });
                                    }
                                })
                                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogAndroid = alertDialogBuilderUserInput.create();
                                alertDialogAndroid.setTitle("Adding CON to '" + lastProfile + "'");
                                alertDialogAndroid.show();
                                if (cons.size() >= 32) {
                                    alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setText(R.string.full);
                                    alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                                }
                                if (empty[0])
                                    alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                            }
                        });
                    }
                });
            }
        });

        // edit mode lock icon
        editmode = findViewById(R.id.lock);
        editmode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean connected = matrix.getConList().length > 0 || matrix.getCpuList().length > 0;
                if (!editing) {
                    editing = true;
                    editmode.setImageResource(R.drawable.unlocked);
                    fullConnect.setEnabled(false);
                    video_only.setEnabled(false);
                    disconnect.setEnabled(false);
                    disconnectAll.setEnabled(false);
                    addConP.setEnabled(true);
                } else {
                    editing = false;
                    editmode.setImageResource(R.drawable.locked);
                    fullConnect.setEnabled(connected);
                    video_only.setEnabled(connected);
                    disconnect.setEnabled(connected);
                    disconnectAll.setEnabled(connected);
                    addConP.setEnabled(false);
                }
                grid.setEdit(editing);
            }
        });

        status = findViewById(R.id.status);
        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                assert connectivityManager != null;
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
                String s;
                if (matrix.getCpuUnits().length > 0 || matrix.getConUnits().length > 0)
                    s = "Connected to the matrix";
                else if (!connected)
                    s = "Check your network connection";
                else
                    s = "Check your matrix settings";
                Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
            }
        });

        // CONs layout on touch listener
        grid.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int lX = touchX;
                int lY = touchY;
                touchX = (int) event.getX();
                touchY = (int) event.getY();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        lastX = touchX;
                        lastY = touchY;
                        System.out.println("Touching down!");
                        for (int i = 0; i < grid.trackers.size(); ++i) {
                            Grid.Tracker t = grid.trackers.get(i);
                            if (t.r.contains(touchX, touchY)) {
                                if (grid.edit)
                                    grid.setDrag(i, true);
                                else
                                    grid.setDown(i);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("Touching up!");
                        if (grid.edit && touchX == lastX && touchY == lastY) {
                            for (int i = 0; i < grid.trackers.size(); ++i) {
                                final int a = i;
                                Rect r = grid.trackers.get(i).r;
                                if (ok && r.contains(touchX, touchY) && grid.trackers.get(i).dragged && !grid.trackers.get(i).help) {
                                    ok = false;
                                    LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ctx);
                                    @SuppressLint("InflateParams") final View mView = layoutInflaterAndroid.inflate(R.layout.editcon, null);
                                    final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ctx);
                                    alertDialogBuilderUserInput.setView(mView);
                                    final Spinner s1 = mView.findViewById(R.id.spinner2);
                                    final Spinner s2 = mView.findViewById(R.id.spinner3);
                                    AsyncTask.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            ConJoin c = CONxProfileDAO.getConById(cons.get(a).id).get(0);
                                            switch (c.width) {
                                                case 46:
                                                    s1.setSelection(0);
                                                    break;
                                                case 69:
                                                    s1.setSelection(1);
                                                    break;
                                                case 92:
                                                    s1.setSelection(2);
                                                    break;
                                                default:
                                                    s1.setSelection(0);
                                            }
                                            switch (c.height) {
                                                case 32:
                                                    s2.setSelection(0);
                                                    break;
                                                case 48:
                                                    s2.setSelection(1);
                                                    break;
                                                case 64:
                                                    s2.setSelection(2);
                                                    break;
                                                default:
                                                    s2.setSelection(0);
                                            }
                                        }
                                    });

                                    alertDialogBuilderUserInput
                                            .setCancelable(false)
                                            .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialogBox, int id) {
                                                    ok = true;
                                                    final String width = s1.getSelectedItem().toString();
                                                    final String height = s2.getSelectedItem().toString();
                                                    AsyncTask.execute(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            ConJoin c = CONxProfileDAO.getConById(cons.get(a).id).get(0);
                                                            switch (width.charAt(0)) {
                                                                case 's':
                                                                    c.width = 46;
                                                                    break;
                                                                case 'm':
                                                                    c.width = 69;
                                                                    break;
                                                                case 'l':
                                                                    c.width = 92;
                                                                    break;
                                                            }
                                                            switch (height.charAt(0)) {
                                                                case 's':
                                                                    c.height = 32;
                                                                    break;
                                                                case 'm':
                                                                    c.height = 48;
                                                                    break;
                                                                case 'l':
                                                                    c.height = 64;
                                                                    break;
                                                            }
                                                            CONxProfileDAO.updateValues(c.id, c.x, c.y, c.width, c.height);
                                                        }
                                                    });
                                                    reloadGridView(matrix);
                                                }
                                            })
                                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    ok = true;
                                                }
                                            })
                                            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                                                @Override
                                                public void onCancel(DialogInterface dialog) {
                                                    ok = true;
                                                }
                                            })
                                            .setNeutralButton("REMOVE CON FROM PROFILE", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    final String profile = dynamic_profile_list.getSelectedItem().toString();
                                                    AlertDialog al = new AlertDialog.Builder(ctx)
                                                            .setCancelable(false)
                                                            .setTitle("Warning")
                                                            .setMessage("Are you sure you want to remove '" + cons.get(a).name + "' from '" + profile + "' ?")
                                                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                                    ok = true;
                                                                    AsyncTask.execute(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            CONxProfileDAO.removeCon(cons.get(a).id);
                                                                        }
                                                                    });
                                                                    reloadGridView(matrix);
                                                                }
                                                            })
                                                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    ok = true;
                                                                }
                                                            })
                                                            .create();
                                                    al.show();
                                                    al.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                                    al.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                                }
                                            });
                                    alertDialogAndroid = alertDialogBuilderUserInput.create();
                                    alertDialogAndroid.setTitle("'" + cons.get(i).name + (cons.get(i).cpu == null ? "' disconnected" : "' connected to '" + cons.get(i).cpu + "'"));
                                    alertDialogAndroid.show();
                                    alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                    alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                    alertDialogAndroid.getButton(AlertDialog.BUTTON_NEUTRAL).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                }
                            }
                        } else {
                            for (int i = 0; i < grid.trackers.size(); ++i) {
                                final int a = i;
                                final Rect r = grid.trackers.get(i).r;
                                if (r.contains(touchX, touchY) && !grid.edit) {
                                    int pos = matrix.getPositionByConId(cons.get(i).name);
                                    grid.up(i);
                                    if (pos >= 0)
                                        matrix.getConUnits()[pos].setSelected(grid.trackers.get(i).selected);
                                } else if (grid.trackers.get(i).help) {
                                    int wdif = r.left % 8;
                                    r.left -= wdif;
                                    r.right -= wdif;
                                    int hdif = r.top % 8;
                                    r.top -= hdif;
                                    r.bottom -= hdif;
                                    grid.invalidate();
                                    AsyncTask.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            ConJoin c = CONxProfileDAO.getConById(cons.get(a).id).get(0);
                                            c.x = r.left;
                                            c.y = r.top;
                                            System.out.println(c.x + " " + c.y);
                                            CONxProfileDAO.updateValues(c.id, c.x, c.y, c.width, c.height);
                                        }
                                    });
                                }
                                grid.setDrag(i, false);
                                grid.trackers.get(i).help = false;
                                grid.trackers.get(i).down = false;
                            }
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        System.out.println("Touching move!");
                        if (grid.edit) {
                            for (int i = 0; i < grid.trackers.size(); ++i) {
                                Grid.Tracker t = grid.trackers.get(i);
                                if (t.dragged) {
                                    t.move(touchX - lX, touchY - lY);
                                    if (touchY != lastY || touchX != lastX)
                                        t.help = true;
                                }
                            }
                            break;
                        }
                        break;
                }
                return true;
            }
        });

        // full connect button
        fullConnect = findViewById(R.id.full_connect);
        fullConnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                matrix.setConnectMode(2);
                new SendMatrixCommand().execute(matrix);
            }
        });

        // video connect button
        video_only = findViewById(R.id.video_only);
        video_only.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                matrix.setPortNumber(portNumber);
                matrix.setIpAddress(ipAddress);
                matrix.setConnectMode(1);
                new SendMatrixCommand().execute(matrix);
            }
        });

        // disconnect button
        disconnect = findViewById(R.id.disconnect);
        disconnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                matrix.setPortNumber(portNumber);
                matrix.setIpAddress(ipAddress);
                matrix.setConnectMode(0);
                new SendMatrixCommand().execute(matrix);
            }
        });

        // disconnect all button
        disconnectAll = findViewById(R.id.disconnectAll);
        disconnectAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                matrix.setPortNumber(portNumber);
                matrix.setIpAddress(ipAddress);
                matrix.setConnectMode(0);
                // select all of profile
                for (int i = 0; i < grid.trackers.size(); ++i) {
                    int pos = matrix.getPositionByConId(cons.get(i).name);
                    matrix.getConUnits()[pos].setSelected(true);
                }
                matrix.setPortNumber(portNumber);
                matrix.setIpAddress(ipAddress);
                new SendMatrixCommand().execute(matrix);
            }
        });

        // profile list spinner
        dynamic_profile_list = findViewById(R.id.dynamic_profile_list);
        dynamic_profile_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView,
                                       final int position, long id) {
                String profile = parentView.getSelectedItem().toString();
                new LoadConsFromProfile().execute(profile);
                lastProfile = profile;
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (login && user != null) {
                            user.lastProfile = position;
                            userdao.update(user);
                        }
                    }
                });
                lastPosition = position;
                btn_delete.setEnabled(true);
                btn_edit.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // 'edit' button
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ctx);
                @SuppressLint("InflateParams") View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ctx);
                alertDialogBuilderUserInput.setView(mView);
                alertDialogBuilderUserInput.setTitle(lastProfile + " [editing]");

                final EditText userInputName = mView.findViewById(R.id.userInputName);
                final EditText userInputInfo = mView.findViewById(R.id.userInputInfo);
                userInputInfo.setEnabled(true);
                userInputName.setEnabled(true);
                final int[] profile_id = new int[1];
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        final Profile p = profileDAO.getProfileFromName(lastProfile)[0];
                        profile_id[0] = p.id;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                userInputName.setText(p.name);
                                userInputInfo.setText(p.information);
                            }
                        });
                    }
                });
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                final String name = userInputName.getText().toString();
                                final String info = userInputInfo.getText().toString();
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        profileDAO.updateNameInfo(profile_id[0], name, info);
                                        new LoadProfiles().execute(matrix);
                                    }
                                });
                            }
                        })
                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                    }
                                });

                final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                userInputName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        alertDialogAndroid.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(s.length() > 0);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }
        });

        // 'delete' button
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String profile = dynamic_profile_list.getSelectedItem().toString();
                AlertDialog a = new AlertDialog.Builder(ctx)
                        .setCancelable(false)
                        .setTitle("Warning")
                        .setMessage("Are you sure you want to delete '" + profile + "' ?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String name = dynamic_profile_list.getSelectedItem().toString();
                                new DeleteProfile(name).execute();
                                cons = new ArrayList<>();
                                rectangles = new ArrayList<>();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        addConP.setEnabled(false);
                                        btn_delete.setEnabled(false);
                                        btn_edit.setEnabled(false);
                                        grid.update(rectangles, cons);
                                    }
                                });
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                a.show();
                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            }
        });

        // 'create' button
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ctx);
                @SuppressLint("InflateParams") View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ctx);
                alertDialogBuilderUserInput.setView(mView);
                alertDialogBuilderUserInput.setTitle("Create a profile");

                final EditText userInputName = mView.findViewById(R.id.userInputName);
                final EditText userInputInfo = mView.findViewById(R.id.userInputInfo);
                userInputInfo.setEnabled(true);
                userInputName.setEnabled(true);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                String name = userInputName.getText().toString();
                                String info = userInputInfo.getText().toString();
                                new AddProfile(name, info).execute();
                            }
                        })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                final Button create = alertDialogAndroid.getButton(DialogInterface.BUTTON_POSITIVE);
                create.setEnabled(false);
                userInputName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        create.setEnabled(s.length() > 0);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (privilege != 0 || username.length() == 0) {
            menu.removeItem(R.id.users);
            menu.removeItem(R.id.resetDb);
        }
        if (!login)
            menu.removeItem(R.id.account);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.refreshMatrix:
                // refresh matrix data
                matrix.setPortNumber(portNumber);
                matrix.setIpAddress(ipAddress);
                new LoadMatrixData().execute(matrix);
                for (int i = 0; i < cpus_actions.getChildCount(); i++) {
                    if (cpus_actions.getChildAt(i) != null)
                        cpus_actions.getChildAt(i).setBackgroundColor(cpus_actions.getSolidColor());
                }
                break;
            case R.id.users:
                Intent intent = new Intent(getApplicationContext(), Users.class);
                intent.putExtra("username", username);
                startActivity(intent);
                break;
            case R.id.account:
                LayoutInflater layinflater = LayoutInflater.from(ctx);
                @SuppressLint("InflateParams") View tmView = layinflater.inflate(R.layout.account, null);
                AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
                dialog.setView(tmView);
                dialog.setTitle(username);
                final EditText old = tmView.findViewById(R.id.old);
                final EditText pwd = tmView.findViewById(R.id.password);
                final EditText pwd2 = tmView.findViewById(R.id.confirm);
                dialog.setCancelable(false)
                        .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                final String ol = old.getText().toString();
                                final String pw = pwd.getText().toString();
                                final String pw2 = pwd2.getText().toString();
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        String oldhash = Login.hash(ol, username);
                                        User[] user = userdao.authenticate(username, oldhash);
                                        if (user.length == 1 && pw.equals(pw2)) {
                                            user[0].password = Login.hash(pw, username);
                                            userdao.update(user[0]);
                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                    }
                                });
                final AlertDialog finale = dialog.create();
                finale.show();
                finale.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                finale.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                pwd.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        finale.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(s.toString().equals(pwd2.getText().toString()));
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                pwd2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        finale.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(s.toString().equals(pwd.getText().toString()));
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                break;
            case R.id.action_settings:
                // open settings dialog
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ctx);
                @SuppressLint("InflateParams")
                View mView = layoutInflaterAndroid.inflate(R.layout.activity_settings, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ctx);
                alertDialogBuilderUserInput.setView(mView);
                alertDialogBuilderUserInput.setTitle("Settings");

                final EditText iport = mView.findViewById(R.id.port);
                final EditText iip = mView.findViewById(R.id.ip);
                final CheckBox cb = mView.findViewById(R.id.autologin);
                if (privilege == 0) {
                    cb.setChecked(login);
                    cb.setVisibility(View.VISIBLE);
                }
                iport.setText(String.valueOf(portNumber));
                iip.setText(ipAddress);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                final String ip = iip.getText().toString();
                                final String port = iport.getText().toString();
                                login = cb.isChecked();
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        portNumber = Integer.parseInt(port);
                                        ipAddress = ip;
                                        settingsDAO.update(ipAddress, portNumber, login);
                                        matrix = new DracoMatrix(ipAddress, portNumber);
                                        new LoadMatrixData().execute(matrix);
                                        checkLogin();
                                    }
                                });
                            }
                        })
                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                    }
                                });
                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                break;
            case R.id.resetDb:
                // reset database
                AlertDialog a = new AlertDialog.Builder(ctx)
                        .setCancelable(false)
                        .setTitle("Warning")
                        .setMessage("Are you sure you want to reset all the profiles and layouts ?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        CONxProfileDAO.delete();
                                        condao.delete();
                                        profileDAO.delete();
                                        cons = new ArrayList<>();
                                        rectangles = new ArrayList<>();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                addConP.setEnabled(false);
                                                btn_delete.setEnabled(false);
                                                btn_edit.setEnabled(false);
                                                grid.update(rectangles, cons);
                                            }
                                        });
                                        lastProfile = "";
                                        lastPosition = 0;
                                        user.lastProfile = 0;
                                        userdao.update(user);
                                        reloadGridView(matrix);
                                    }
                                });
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .create();
                a.show();
                a.getButton(AlertDialog.BUTTON_POSITIVE).

                        setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                a.getButton(AlertDialog.BUTTON_NEGATIVE).

                        setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // update data and cpus views
    protected void reloadGridView(DracoMatrix m) {
        matrix = new DracoMatrix(m);
        new LoadConsFromProfile().execute(lastProfile);
        new LoadProfiles().execute(m);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if (matrix.getCpuUnits().length > 0 || matrix.getConUnits().length > 0) {
                    CPU[] cpuss = cpudao.getAll();
                    for (Extender ext : matrix.getCpuUnits()) {
                        boolean found = false;
                        for (CPU cpu : cpuss) {
                            if (Arrays.equals(cpu.id, ext.getId())) {
                                if (!cpu.name.equals(ext.getName())) {
                                    cpu.name = ext.getName();
                                    cpudao.updateCpu(cpu);
                                }
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            cpudao.addCpu(new CPU(ext.getId(), ext.getName(), (short) 0));
                    }
                    for (CPU cpu : cpuss) {
                        boolean removed = true;
                        for (Extender ext : matrix.getCpuUnits())
                            if (Arrays.equals(cpu.id, ext.getId())) {
                                ext.cpuType = cpu.type;
                                removed = false;
                                break;
                            }
                        if (removed)
                            cpudao.remove(cpu.id);
                    }
                }
                CPU[] tmp2 = cpudao.getAll();
                cpus = new ArrayList<>();
                cpus.addAll(Arrays.asList(tmp2));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        HashSet<Short> usedCpus = matrix.getConnectedCpus();
                        final ImageAdapter imageAdapterCPU = new ImageAdapter(ctx, matrix, usedCpus);
                        cpus_actions.setAdapter(imageAdapterCPU);
                        cpus_actions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> parent, View v,
                                                    final int position, long id) {
                                if (grid.edit) {
                                    AsyncTask.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            CPU ext = cpus.get(position);
                                            short newType = (short) ((ext.type + 1) % 3);
                                            System.out.println("type: " + newType);
                                            cpudao.updateCpu(new CPU(ext.id, ext.name, newType));
                                            if (matrix.getCpuUnits().length > position)
                                                matrix.getCpuUnits()[position].cpuType = newType;
                                            cpus.get(position).type = newType;
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    imageAdapterCPU.notifyDataSetChanged();
                                                }
                                            });
                                            System.out.println(newType);
                                        }
                                    });
                                } else if (imageAdapterCPU.getMatrixGrid().getCpuUnits().length > position) {
                                    imageAdapterCPU.getMatrixGrid().selectExtender(imageAdapterCPU.getMatrixGrid().getCpuUnits()[position]);
                                    imageAdapterCPU.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    // update data from matrix
    protected void reloadMatrixData(DracoMatrix m) {
        if (m.getConList().length == 0 && m.getCpuList().length == 0) {
            status.setImageResource(R.drawable.offline);
            fullConnect.setEnabled(false);
            video_only.setEnabled(false);
            disconnect.setEnabled(false);
            disconnectAll.setEnabled(false);
        } else {
            status.setImageResource(R.drawable.online);
            fullConnect.setEnabled(!editing);
            video_only.setEnabled(!editing);
            disconnect.setEnabled(!editing);
            disconnectAll.setEnabled(!editing);
            addConP.setEnabled(editing);
        }
    }

    // load the cons from the current profile into the canvas
    @SuppressLint("StaticFieldLeak")
    private class LoadConsFromProfile extends AsyncTask<String, Integer, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            Profile[] profiles = profileDAO.getProfileFromName(strings[0]);
            if (profiles.length > 0) {
                List<ConJoin> tmp = CONxProfileDAO.getConsByProfile(profiles[0].id);
                cons = new ArrayList<>(tmp);
                rectangles = new ArrayList<>();
                for (ConJoin c : cons) {
                    rectangles.add(new Rect(c.x, c.y, c.width, c.height));
                    byte[] id = matrix.getCpuIdByConName(c.name);
                    if (id != null)
                        c.cpu = matrix.getExtenderNameUsingId(id);
                    else
                        c.cpu = null;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        grid.update(rectangles, cons);
                    }
                });
            }
            return null;
        }
    }

    // delete a profile
    @SuppressLint("StaticFieldLeak")
    private class DeleteProfile extends AsyncTask<Void, Integer, Boolean> {
        private String name;

        DeleteProfile(String name) {
            this.name = name;
        }

        @Override
        protected Boolean doInBackground(Void... ms) {
            Profile[] get = profileDAO.getProfileFromName(name);
            CONxProfileDAO.removeProfile(get[0].id);
            profileDAO.deleteByName(name);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean delete) {
            super.onPostExecute(delete);
            if (delete) {
                profile_list.remove(name);
                profile_list.notifyDataSetChanged();
                dynamic_profile_list.setAdapter(profile_list);
                dynamic_profile_list.setSelection(dynamic_profile_list.getCount() - 1);
            }
        }
    }

    // create a profile
    @SuppressLint("StaticFieldLeak")
    private class AddProfile extends AsyncTask<Void, Integer, Boolean> {
        private String name;
        private String information;

        AddProfile(String name, String information) {
            this.name = name;
            this.information = information;
        }

        @Override
        protected Boolean doInBackground(Void... ms) {
            Profile[] tmp = profileDAO.getProfileFromName(name);
            if (tmp.length > 0)
                return false;
            Integer id = username.length() == 0 ? null : user.id;
            profileDAO.addProfile(new Profile(name, information, id));
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                profile_list.add(name);
                profile_list.notifyDataSetChanged();
                dynamic_profile_list.setAdapter(profile_list);
                dynamic_profile_list.setSelection(dynamic_profile_list.getCount() - 1);
            }
        }
    }

    // load the profiles from the matrix to the database and the views
    @SuppressLint("StaticFieldLeak")
    private class LoadProfiles extends AsyncTask<DracoMatrix, Integer, DracoMatrix> {
        @Override
        protected DracoMatrix doInBackground(DracoMatrix... ms) {
            if (ms.length == 0)
                return null;
            DracoMatrix m = new DracoMatrix(ms[0]);
            Profile[] tab;
            if (privilege == 0)
                tab = profileDAO.getProfiles();
            else
                tab = profileDAO.getProfilesOfUser(user.id);
            for (Extender ext : m.getConUnits()) {
                CON[] c = condao.getConById(ext.getId());
                if (c.length == 0) {
                    CON con = new CON(ext.getId(), ext.getName());
                    condao.addCon(con);
                } else if (!c[0].name.equals(ext.getName())) {
                    c[0].name = ext.getName();
                    condao.updateCon(c[0]);
                }
            }
            if (m.getCpuUnits().length > 0 || m.getConUnits().length > 0) {
                CON[] tmp = condao.getCons();
                for (CON c : tmp) {
                    boolean remove = true;
                    for (Extender ext : m.getConUnits()) {
                        if (Arrays.equals(c.id, ext.getId())) {
                            remove = false;
                            break;
                        }
                    }
                    if (remove)
                        condao.deleteById(c.id);
                }
            }
            dynamic_profile_list = findViewById(R.id.dynamic_profile_list);
            List<String> profiles = new ArrayList<>();
            for (Profile p : tab) {
                profiles.add(p.name);
            }
            profile_list = new ArrayAdapter<>(getBaseContext(), R.layout.spinner_item, profiles);
            profile_list.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dynamic_profile_list.setAdapter(profile_list);
                    dynamic_profile_list.setSelection(lastPosition);
                }
            });
            return m;
        }
    }

    // load data from the matrix
    @SuppressLint("StaticFieldLeak")
    public class LoadMatrixData extends AsyncTask<DracoMatrix, Integer, DracoMatrix> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loading.setVisibility(View.VISIBLE);
                }
            });
        }

        @Override
        protected DracoMatrix doInBackground(DracoMatrix... ms) {
            if (ms.length == 0)
                return null;
            DracoMatrix m = new DracoMatrix(ms[0]);
            m.resetCons();
            m.resetCpus();
            m.resetExtenders();
            m.setSocket();

            if (m.getSocket() != null) {
                m.loadCpuList();
                m.loadConList();
                m.setConnectedCpu();
            }
            return m;
        }

        @Override
        protected void onPostExecute(DracoMatrix m) {
            if (m != null)
                reloadMatrixData(m);
            if (m != null)
                reloadGridView(m);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loading.setVisibility(View.GONE);
                }
            });
        }
    }

    // send a command to the matrix
    @SuppressLint("StaticFieldLeak")
    public class SendMatrixCommand extends AsyncTask<DracoMatrix, Integer, DracoMatrix> {
        @Override
        protected DracoMatrix doInBackground(DracoMatrix... ms) {
            if (ms.length == 0)
                return null;
            DracoMatrix m = new DracoMatrix(ms[0]);
            m.setSocket();

            if (m.getSocket() != null) {
                m.connectConsToCpu(m.selectedCons(), m.selectedCpu(), m.getConnectMode());
                m.setConnectedCpu();
                m.deselectAll();
                for (Grid.Tracker t : grid.trackers)
                    t.selected = false;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        grid.invalidate();
                    }
                });
            }

            m.setSocket();
            if (m.getSocket() != null) {
                m.setConnectedCpu();
            }
            return m;
        }

        @Override
        protected void onPostExecute(DracoMatrix m) {
            reloadGridView(m);
        }
    }

    // CPUs listview custom adapter
    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private DracoMatrix matrixGrid;
        private HashSet<Short> set;

        ImageAdapter(Context c, DracoMatrix m, HashSet<Short> s) {
            mContext = c;
            matrixGrid = new DracoMatrix(m);
            set = s;
        }

        DracoMatrix getMatrixGrid() {
            return matrixGrid;
        }

        public int getCount() {
            return cpus.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        @SuppressLint({"InflateParams", "SetTextI18n"})
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View extenderView;

            ImageView cpuIcon;
            TextView cpuName;
            if (convertView == null || position < cpus.size()) {
                // if it's not recycled, initialize some attributes
                assert inflater != null;
                extenderView = inflater.inflate(R.layout.extender, null);
                cpuIcon = extenderView.findViewById(R.id.grid_item_image); //new ImageView(mContext);
                cpuIcon.setScaleType(ImageView.ScaleType.FIT_CENTER);
                cpuName = extenderView.findViewById(R.id.grid_item_label);
                if (matrixGrid.getCpuUnits().length > position)
                    matrixGrid.getCpuUnits()[position].setPosition(position);
                cpuName.setText(cpus.get(position).name);
                ByteBuffer wrap = ByteBuffer.wrap(cpus.get(position).id);
                boolean connected = set.contains(wrap.getShort());
                switch (cpus.get(position).type) {
                    case 0:
                        cpuIcon.setImageResource(connected ? R.drawable.computer_green : R.drawable.computer);
                        break;
                    case 1:
                        cpuIcon.setImageResource(connected ? R.drawable.cam_green : R.drawable.cam);
                        break;
                    case 2:
                        cpuIcon.setImageResource(connected ? R.drawable.remote_green : R.drawable.remote);
                        break;
                    default:
                        break;
                }
                if (matrixGrid.getCpuUnits().length > position && matrixGrid.getCpuUnits()[position].isSelected())
                    cpuName.setTextColor(Color.BLUE);
            } else {
                extenderView = convertView;
            }
            return extenderView;
        }
    }
}
