package com.elecdan.draco_tera_connect.myUtilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by Moctar on 27/03/2017.
 */

public final class NetworkUtilities {
    public static Socket connectMatrix(String _address, int _portNumber) {
        try {
            Socket socket = new Socket(_address, _portNumber);
            return socket;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] cpuList(Socket socket) {
        try {
            final InputStream is = socket.getInputStream();
            final OutputStream os = socket.getOutputStream();
            byte[] b = new byte[8192];
            int endOfLine;
            // Get CPU List
            os.write(0x1B); // ESP
            os.write(0x5B); // [
            os.write(0x67); // g
            os.write(0x07); // LSB Size
            os.write(0x00); // MSB Size
            os.write(0x00); // LSB First CPU ID (ID=0000)
            os.write(0x00); // MSB First CPU ID
            os.flush();
            endOfLine = is.read(b); // Read the stream
            if (endOfLine != -1) { // No data available
                for (int i = 9; i < endOfLine; i += 24) {
                    //Each device ID consist of 2 byte with LSB the first byte and the MSB the second one
                    //We first get the CPU ID then its name
                    System.out.print(String.format("%02x", b[i + 1]));
                    System.out.print(String.format("%02x", b[i]));
                    System.out.print(" ");
                    //The name of the device consist of 24 bytes
                    for (int j = i + 2; j < i + 24; j++) {
                        System.out.print(String.format("%c", b[j]));
                    }
                    System.out.println();
                }
            } else {
                System.out.println("No available data");
            }
            System.out.println("Finished reading");
            // End Get CPU List
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] conList(Socket socket) {
        try {
            final InputStream is = socket.getInputStream();
            final OutputStream os = socket.getOutputStream();
            byte[] b = new byte[8192];
            int endOfLine;
            // Get CON List
            os.write(0x1B); // ESP
            os.write(0x5B); // [
            os.write(0x68); // h
            os.write(0x07); // LSB Size
            os.write(0x00); // MSB Size
            os.write(0x00); // LSB First CON ID (ID=0000)
            os.write(0x00); // MSB First CON ID
            os.flush();
            endOfLine = is.read(b); // Read the stream
            if (endOfLine != -1) { // No data available
                for (int i = 9; i < endOfLine; i += 24) {
                    //Each device ID consist of 2 byte with LSB the first byte and the MSB the second one
                    //We first get the CON ID then its name
                    System.out.print(String.format("%02x", b[i + 1]));
                    System.out.print(String.format("%02x", b[i]));
                    System.out.print(" ");
                    //The name of the device consist of 24 bytes
                    for (int j = i + 2; j < i + 24; j++) {
                        System.out.print(String.format("%c", b[j]));
                    }
                    System.out.println();
                }
            } else {
                System.out.println("No available data");
            }
            System.out.println("Finished reading");
            // End Get CON List
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void extendList() {

    }
}
