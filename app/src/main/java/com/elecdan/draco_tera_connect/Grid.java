package com.elecdan.draco_tera_connect;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.elecdan.draco_tera_connect.db.ConJoin;

import java.util.ArrayList;

public class Grid extends View {
    Paint p, tp;
    public ArrayList<Tracker> trackers;
    ArrayList<ConJoin> cons;
    boolean edit;
    float a, b;
    Context ctx;

    class Tracker {
        boolean selected, down, dragged, help;
        Rect r;
        int width;

        Tracker(int left, int top, int right, int bottom, int w) {
            selected = false;
            down = false;
            dragged = false;
            help = false;
            width = w;
            this.r = new Rect(left, top, right, bottom);
        }

        void move(int x, int y) {
            int left = r.left + x;
            int right = r.right + x;
            int top = r.top + y;
            int bottom = r.bottom + y;
            if (left > 0 && top > 0 && left < getWidth() - a && top < getHeight() - b) {
                r.left = left;
                r.right = right;
                r.top = top;
                r.bottom = bottom;
            }
            invalidate();
        }
    }

    public Grid(Context cxt, AttributeSet attrs) {
        super(cxt, attrs);
        ctx = cxt;
        p = new Paint();
        tp = new Paint();
        edit = false;
        trackers = new ArrayList<>();
        cons = new ArrayList<>();
    }

    void setDrag(int i, boolean d) {
        for (int a = 0; a < trackers.size(); ++a)
            trackers.get(a).dragged = false;
        trackers.get(i).dragged = d;
        invalidate();
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas cv) {
        a = getWidth() / 8;
        b = getHeight() / 4;
        if (edit) {
            p.setStyle(Paint.Style.STROKE);
            p.setStrokeWidth(1);
            p.setColor(Color.LTGRAY);
            for (int i = 0; i < getWidth(); i += 8)
                cv.drawLine(i, 0, i, getHeight(), p);
            for (int i = 0; i < getHeight(); i += 8)
                cv.drawLine(0, i, getWidth(), i, p);
            p.setStrokeWidth(2);
            p.setColor(Color.BLUE);
            cv.drawRect(new Rect(1, 1, getWidth() - 1, getHeight() - 1), p);
        }
        if (trackers.size() > 0) {
            for (int i = 0; i < trackers.size(); ++i) {
                Tracker tmp = trackers.get(i);
                if (tmp.help) {
                    p.setStyle(Paint.Style.STROKE);
                    p.setStrokeWidth(2);
                    p.setColor(Color.RED);
                    cv.drawLine(0, tmp.r.top, getWidth(), tmp.r.top, p);
                    cv.drawLine(0, tmp.r.bottom, getWidth(), tmp.r.bottom, p);
                    cv.drawLine(tmp.r.left, 0, tmp.r.left, getHeight(), p);
                    cv.drawLine(tmp.r.right, 0, tmp.r.right, getHeight(), p);
                }
                if (edit) {
                    p.setStyle(Paint.Style.STROKE);
                    p.setStrokeWidth(2);
                    p.setColor(Color.BLUE);
                    cv.drawRect(tmp.r, p);
                } else if (trackers.get(i).selected) {
                    p.setStyle(Paint.Style.STROKE);
                    p.setStrokeWidth(2);
                    p.setColor(Color.BLUE);
                    int width = (tmp.r.right - tmp.r.left) / 5;
                    int height = (tmp.r.bottom - tmp.r.top) / 5;
                    int left = tmp.r.left - 1;
                    int right = tmp.r.right + 1;
                    int top = tmp.r.top - 1;
                    int bottom = tmp.r.bottom + 1;
                    cv.drawLine(left, top, left + width, top, p);
                    cv.drawLine(left, top, left, top + height, p);
                    cv.drawLine(right, top, right - width, top, p);
                    cv.drawLine(right, top, right, top + height, p);
                    cv.drawLine(left, bottom, left + width, bottom, p);
                    cv.drawLine(left, bottom, left, bottom - height, p);
                    cv.drawLine(right, bottom, right - width, bottom, p);
                    cv.drawLine(right, bottom, right, bottom - height, p);
                }
                System.out.println(cons.size() + " " + trackers.size());
                String cpu = cons.get(i).cpu;
                //icon
                Drawable d = getResources().getDrawable(cpu != null ? R.drawable.monitor_green : R.drawable.monitor);
                d.setBounds(tmp.r.left + 2, tmp.r.top + 2, tmp.r.right - 2, tmp.r.bottom - 2);
                d.draw(cv);
                //texts
                tp.setStyle(Paint.Style.FILL);
                tp.setTypeface(Typeface.createFromAsset(ctx.getAssets(), "fonts/Monospaced.ttf"));
                tp.setColor(Color.WHITE);
                tp.setTextSize(15 * getResources().getDisplayMetrics().density);
                tp.setAntiAlias(true);
                int width = tmp.r.right - tmp.r.left;
                float unit = tp.measureText("sixteencharacter") / 16;
                int max = (int) (width / unit) - 1;
                if (cpu != null) {
                    int len = cons.get(i).name.length() < max ? cons.get(i).name.length() : max;
                    cv.drawText(cons.get(i).name.substring(0, len), tmp.r.left + (width - unit * len) / 2, tmp.r.top + (tmp.r.bottom - tmp.r.top) / 3, tp);
                    len = cpu.length() < max ? cpu.length() : max;
                    cv.drawText(cpu.substring(0, len), tmp.r.left + (width - unit * len) / 2, tmp.r.top + 2 * (tmp.r.bottom - tmp.r.top) / 3, tp);
                } else {
                    int len = cons.get(i).name.length() < max ? cons.get(i).name.length() : max;
                    cv.drawText(cons.get(i).name.substring(0, len), tmp.r.left + (width - unit * len) / 2, tmp.r.top + (tmp.r.bottom - tmp.r.top) / 2, tp);
                }
            }
        }
    }

    public void update(ArrayList<Rect> arr, ArrayList<ConJoin> cons) {
        this.cons = cons;
        trackers = new ArrayList<>();
        for (Rect tmp : arr) {
            float width = tmp.right / 48f;
            float height = tmp.bottom / 32f;
            float left = tmp.left;
            float top = tmp.top;
            float right = tmp.left + width * a;
            float bottom = tmp.top + height * b;
            int w = tmp.right;
            trackers.add(new Tracker((int) (left), (int) (top), (int) (right), (int) (bottom), w));
        }
        invalidate();
    }

    public void setDown(int i) {
        for (int a = 0; a < trackers.size(); ++a)
            trackers.get(a).down = false;
        trackers.get(i).down = true;
        invalidate();
    }

    public void setEdit(boolean b) {
        edit = b;
        invalidate();
    }

    public void up(int i) {
        Tracker t = trackers.get(i);
        if (t.down) {
            t.down = false;
            t.selected ^= true;
        }
        invalidate();
    }
}
