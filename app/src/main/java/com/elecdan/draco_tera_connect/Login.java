package com.elecdan.draco_tera_connect;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.elecdan.draco_tera_connect.db.AppDatabase;
import com.elecdan.draco_tera_connect.db.Group;
import com.elecdan.draco_tera_connect.db.GroupDao;
import com.elecdan.draco_tera_connect.db.SettingsDAO;
import com.elecdan.draco_tera_connect.db.User;
import com.elecdan.draco_tera_connect.db.UserDao;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.appcompat.app.AppCompatActivity;

public class Login extends AppCompatActivity {
    AppDatabase db;
    UserDao userdao;
    SettingsDAO settingsdao;
    GroupDao groupdao;
    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                db = AppDatabase.getInstance(getApplicationContext());
                userdao = db.UserDao();
                settingsdao = db.SettingsDAO();
                groupdao = db.GroupDao();
                if (settingsdao.get().length == 0 || !settingsdao.get()[0].login) {
                    startMain(0, "");
                }
            }
        });
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_GO:
                        login();
                        return true;
                    default:
                        return false;
                }
            }
        });

        Button login = findViewById(R.id.btn_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    void login() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                String un = username.getText().toString();
                String pw = hash(password.getText().toString(), un);
                User[] matches = userdao.authenticate(un, pw);
                if (matches.length == 1) {
                    Group grp = groupdao.getById(matches[0].group_id)[0];
                    startMain(grp.privilege, matches[0].username);
                    System.out.println("START");
                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String hash(String passwordToHash, String username) {
        String generatedPassword = null;
        String salt = "sKbD7XvEhg80ugWUHeVw" + username;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public void startMain(int privilege, String username) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("privilege", privilege);
        intent.putExtra("username", username);
        startActivity(intent);
        finish();
    }
}
