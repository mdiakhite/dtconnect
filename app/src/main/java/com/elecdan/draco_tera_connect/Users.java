package com.elecdan.draco_tera_connect;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.elecdan.draco_tera_connect.db.AppDatabase;
import com.elecdan.draco_tera_connect.db.Group;
import com.elecdan.draco_tera_connect.db.GroupDao;
import com.elecdan.draco_tera_connect.db.User;
import com.elecdan.draco_tera_connect.db.UserDao;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class Users extends AppCompatActivity {
    ListView userList, groupList;
    ImageButton addUser, editUser, deleteUser, addGroup, editGroup, deleteGroup;
    UserDao userdao;
    GroupDao groupdao;
    AppDatabase db;
    String username;
    User[] users;
    int group = -1, user = -1;
    Group[] groups;
    final Context ctx = this;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users);
        addUser = findViewById(R.id.btn_create);
        editUser = findViewById(R.id.btn_edit);
        deleteUser = findViewById(R.id.btn_delete);
        addGroup = findViewById(R.id.btn_createg);
        editGroup = findViewById(R.id.btn_editg);
        deleteGroup = findViewById(R.id.btn_deleteg);
        userList = findViewById(R.id.userList);
        groupList = findViewById(R.id.groupList);
        db = AppDatabase.getInstance(getApplicationContext());
        userdao = db.UserDao();
        groupdao = db.GroupDao();
        intent = getIntent();
        username = intent.getStringExtra("username");
        getSupportActionBar().setTitle(username);

        getGroups();

        addGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ctx);
                View mView = layoutInflaterAndroid.inflate(R.layout.group_create, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ctx);
                alertDialogBuilderUserInput.setView(mView);
                alertDialogBuilderUserInput.setTitle("Create group");

                final EditText groupname = mView.findViewById(R.id.groupname);
                final Switch admin = mView.findViewById(R.id.switch1);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                final String groupn = groupname.getText().toString();
                                final int adm = admin.isChecked() ? 0 : 1;
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        addGroup(groupn, adm);
                                    }
                                });
                            }
                        })
                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                    }
                                });

                final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                groupname.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(final CharSequence s, int start, int before, int count) {
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                String str = s.toString();
                                final boolean enabled = str.length() > 0 && groupdao.getByName(str).length == 0;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(enabled);
                                    }
                                });
                            }
                        });
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }
        });

        editGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ctx);
                View mView = layoutInflaterAndroid.inflate(R.layout.group_create, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ctx);
                alertDialogBuilderUserInput.setView(mView);
                alertDialogBuilderUserInput.setTitle("Edit group");

                final EditText groupname = mView.findViewById(R.id.groupname);
                final Switch admin = mView.findViewById(R.id.switch1);
                groupname.setText(groups[group].name);
                admin.setChecked(groups[group].privilege == 0);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                final String groupn = groupname.getText().toString();
                                final int adm = admin.isChecked() ? 0 : 1;
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        editGroup(groupn, adm);
                                    }
                                });
                            }
                        })
                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                    }
                                });

                final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (admin.isChecked() && userdao.getAdmins().length - users.length < 1)
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    admin.setEnabled(false);
                                }
                            });
                    }
                });
                groupname.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(final CharSequence s, int start, int before, int count) {
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                String str = s.toString();
                                final boolean enabled = str.length() > 0 && groupdao.getByName(str).length == 0;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(enabled);
                                    }
                                });
                            }
                        });
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                admin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                String str = groupname.getText().toString();
                                final boolean enabled = str.length() > 0 && groupdao.getByName(str).length == 0 || isChecked != (groups[group].privilege == 0);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(enabled);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

        deleteGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog a = new AlertDialog.Builder(ctx)
                        .setCancelable(false)
                        .setTitle("Warning")
                        .setMessage("Are you sure you want to delete '" + groups[group].name + "' ?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (groups[group].privilege == 0 && userdao.getAdmins().length - users.length < 1) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "You cannot delete the last admin", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            return;
                                        }
                                        groupdao.delete(groups[group]);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                addUser.setVisibility(View.INVISIBLE);
                                                editUser.setVisibility(View.INVISIBLE);
                                                deleteUser.setVisibility(View.INVISIBLE);
                                            }
                                        });
                                        group = -1;
                                        user = -1;
                                        for (User u : users)
                                            if (u.username.equals(username))
                                                finish();
                                        getGroups();
                                        getUsers();
                                    }
                                });
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .create();
                a.show();
                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            }
        });

        deleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog a = new AlertDialog.Builder(ctx)
                        .setCancelable(false)
                        .setTitle("Warning")
                        .setMessage("Are you sure you want to delete '" + users[user].username + "' ?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (groups[group].privilege == 0 && userdao.getAdmins().length == 1) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "You cannot delete the last admin", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            return;
                                        }
                                        if (users[user].username.equals(username)) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    AlertDialog b = new AlertDialog.Builder(ctx)
                                                            .setCancelable(false)
                                                            .setTitle("Warning")
                                                            .setMessage("Are you sure you want to delete your own account ?")
                                                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                                    AsyncTask.execute(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            userdao.delete(users[user]);
                                                                            finish();
                                                                        }
                                                                    });
                                                                }
                                                            })
                                                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                }
                                                            })
                                                            .create();
                                                    b.show();
                                                    b.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                                    b.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                                }
                                            });
                                        } else {
                                            userdao.delete(users[user]);
                                            user = -1;
                                            getUsers();
                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .create();
                a.show();
                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            }
        });

        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(ctx);
                View mView = layoutInflaterAndroid.inflate(R.layout.user_create, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ctx);
                alertDialogBuilderUserInput.setView(mView);
                alertDialogBuilderUserInput.setTitle("Create user");

                final EditText usernameInput = mView.findViewById(R.id.username);
                final EditText passwordInput = mView.findViewById(R.id.password);
                final EditText confirmInput = mView.findViewById(R.id.confirm_password);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                final String username = usernameInput.getText().toString();
                                final String pwd1 = passwordInput.getText().toString();
                                addUser(username, pwd1);
                            }
                        })
                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                    }
                                });

                final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                usernameInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(final CharSequence s, int start, int before, int count) {
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                String str = s.toString();
                                final boolean enabled = passwordInput.getText().toString().equals(confirmInput.getText().toString()) && str.length() > 0 && userdao.get(str).length == 0;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        alertDialogAndroid.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(enabled);
                                    }
                                });
                            }
                        });
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                confirmInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(final CharSequence s, int start, int before, int count) {
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                String str = usernameInput.getText().toString();
                                final boolean enabled = passwordInput.getText().toString().equals(s.toString()) && str.length() > 0 && userdao.get(str).length == 0;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        alertDialogAndroid.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(enabled);
                                    }
                                });
                            }
                        });
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                passwordInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(final CharSequence s, int start, int before, int count) {
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                String str = usernameInput.getText().toString();
                                final boolean enabled = s.toString().equals(confirmInput.getText().toString()) && str.length() > 0 && userdao.get(str).length == 0;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        alertDialogAndroid.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(enabled);
                                    }
                                });
                            }
                        });
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

            }
        });
    }

    void addUser(final String username, final String password) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                User u = new User(groups[group].id, username, Login.hash(password, username));
                try {
                    userdao.add(u);
                } catch (SQLiteConstraintException ignored) {
                }
                getUsers();
            }
        });
    }

    void editGroup(final String groupname, final int privilege) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    groups[group].privilege = privilege;
                    groups[group].name = groupname;
                    groupdao.update(groups[group]);
                } catch (SQLiteConstraintException ignored) {
                }
                getGroups();
            }
        });
    }

    void addGroup(final String groupname, final int privilege) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    groupdao.add(new Group(privilege, groupname));
                } catch (SQLiteConstraintException ignored) {
                }
                getGroups();
            }
        });
    }

    void getGroups() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                groups = groupdao.getAll();
                final List<String> items = new ArrayList<>();
                for (Group g : groups)
                    items.add(g.name);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final ArrayAdapter<String> list = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, items) {
                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView text = view.findViewById(android.R.id.text1);
                                text.setTextColor(position == group ? Color.BLUE : Color.BLACK);
                                return view;
                            }
                        };
                        groupList.setAdapter(list);
                        boolean ok = group != -1 && items.size() > 0;
                        editGroup.setVisibility(ok ? View.VISIBLE : View.INVISIBLE);
                        deleteGroup.setVisibility(ok ? View.VISIBLE : View.INVISIBLE);
                        groupList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (group == position) {
                                    group = -1;
                                    addUser.setVisibility(View.INVISIBLE);
                                    editUser.setVisibility(View.INVISIBLE);
                                    deleteUser.setVisibility(View.INVISIBLE);
                                    editGroup.setVisibility(View.INVISIBLE);
                                    deleteGroup.setVisibility(View.INVISIBLE);
                                } else {
                                    group = position;
                                    addUser.setVisibility(View.VISIBLE);
                                    editGroup.setVisibility(View.VISIBLE);
                                    deleteGroup.setVisibility(View.VISIBLE);
                                }
                                user = -1;
                                getUsers();
                                list.notifyDataSetChanged();
                            }
                        });
                    }
                });
            }
        });
    }

    void getUsers() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if (group == -1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            userList.setAdapter(null);
                        }
                    });
                    return;
                }
                users = userdao.getByGroup(groups[group].id);
                final List<String> items = new ArrayList<>();
                for (User u : users)
                    items.add(u.username);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final ArrayAdapter<String> list = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, items) {
                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView text = view.findViewById(android.R.id.text1);
                                text.setTextColor(user == position ? Color.BLUE : Color.BLACK);
                                return view;
                            }
                        };
                        userList.setAdapter(list);
                        editUser.setVisibility(user != -1 && items.size() > 0 ? View.VISIBLE : View.INVISIBLE);
                        deleteUser.setVisibility(user != -1 && items.size() > 0 ? View.VISIBLE : View.INVISIBLE);
                        userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (user == position) {
                                    editUser.setVisibility(View.INVISIBLE);
                                    deleteUser.setVisibility(View.INVISIBLE);
                                    user = -1;
                                } else {
                                    user = position;
                                    editUser.setVisibility(View.VISIBLE);
                                    deleteUser.setVisibility(View.VISIBLE);
                                }
                                list.notifyDataSetChanged();
                            }
                        });
                    }
                });
            }
        });
    }
}
